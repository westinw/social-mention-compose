# Mention Composer App

An input component that will suggest social media accounts when typing usernames preceeded by a special character (usually @). The editor will not begin rendering suggestions until you have typed at least two characters after the mention.

See gifs of demos in `/demos` directory which shows simple examples with suggestionCount=2.


## Prelude
Built using [create-react-app](https://github.com/facebookincubator/create-react-app) to get a demo off the ground quickly. Normally I would like to see something as resuable as this in an npm package. In order to make this an npm package we can leverage webpack to create a umd build and for an es module build we can simply use babel + the `module` field in `package.json`. There are many ways to optimize and approach the build for a library if unique scenarios arise, which I would be happy to discuss.

A simple way to handle styles if this became a library, would be to keep them in CSS and use `extract-text-plugin` or use a cool library like [styled-components](https://github.com/styled-components/styled-components).

See [Possible Performance Improvements](#Possible-Performance-Improvements) for information on making this more performant if needed.

## Installation and Running
Make sure you have [yarn](https://yarnpkg.com/en/docs/install) or npm installed.

The api base will default to `http:localhost:3001` but you can set the `REACT_APP_API_BASE` environment variable to override.

### Yarn (preferred)

* `yarn`
* `yarn start` starts a local dev server on port 3000.
* `yarn test` to test. for coverage run `yarn test -- --coverage`

### npm

* `npm install`
* `npm start`
* `npm test` and `npm test -- --coverage`

## Components

### Composer
A textarea input that allows you to call out social mentions and autocomplete them via a fetch call (or any promise really).


#### Props

| Name | Type   	| Default   	| Description |
|---         |---	   |---	|---	|---|
|cache       |boolean|true|cache suggestion requests|
|debounceTime|number|200| debounce suggestion requests|
| fetch  	   |Function -> Promise|identity|called to get suggestions|
| focusIndex |number|-1|Which suggestion to focus on render|
| mentionChar|character|'@'|The mention delimiter|
| onKeyDown  |Function|identity| keydown callback|
| resetFocus |Function|identity|suggestion focus clearing callback|
| suggestionCount|number|4|How many suggestions to render|
| suggestionRenderer|Function->Component|identity(required)| Component to render the suggestions in. Passes all data from suggetion fetch item|

#### Example Usage

```jsx
class App extends Component {
  // fetch function to retrieve data
  fetchUsers(username) {
    return fetch('url').then(data => data.json());
  }

  // suggestion item renderer
  itemRenderer({ avatar, focused, name, onSelect, username }) {
    return (
      <ListItem
        key={username}
        avatar={avatar}
        focused={focused}
        name={name}
        onSelect={onSelect}
        username={username}
      />
    );
  }

  render() {
    return (
      <div className="text-area-container">
        <Composer
          fetch={this.fetchUsers}
          suggestionRenderer={this.itemRenderer}
        />
      </div>
    );
  }
}
```

#### Dependencies
* [**lodash.debounce**](https://www.npmjs.com/package/lodash.debounce) - for debouncing function
* [**invariant**](https://github.com/zertosh/invariant) - for error messaging to user
* [**jest**](https://facebook.github.io/jest/) - for testing framework and snapshot testing :)
* [**enzyme**](https://github.com/airbnb/enzyme) - for test rendering
* [**create-react-app**](https://github.com/facebookincubator/create-react-app) - to offload all build configurations
* [**prettier**](https://github.com/jlongster/prettier) - for code formatting (not in package.json as its an editor tool)
* [**yarnpkg**](https://yarnpkg.com/en/) for deteministic installs of node_modules


### ArrowKeyStepper

Allows use of arrow keys to navigate through the list of suggestions. User can press the enter to key to accept the highlighted suggestion. This utility is very generic and makes no assuption that its child function will return Composer so it can likely be reused in other ways.

ArrowKeyStepper is built leveraging function as child components as so ArrowKeyStepper can manage the state of selected index and listen for the arrow key events and the child can be agnostic to that.

#### Example Usage

```jsx
<ArrowKeyHandler>
  {({ focusIndex, onKeyDown, resetFocus }) => (
    <Composer
      focusIndex={focusIndex}
      onKeyDown={onKeyDown}
      resetFocus={resetFocus}
      fetch={this.fetchUsers}
      suggestionRenderer={this.itemRenderer}
    />
  )}
</ArrowKeyHandler>
```

### Utils
This could get split out to their own files to clean things up and make them more pronounced so folks don't miss any of them. As the number of the utility functions was small and this would normally be intended as an npm module on its own, I decided to keep them in one file for the time being.


## Testing

I leveraged Jest for unit testing as I really enjoy the snapshot functioanlity and watch mode that runs only over changed files. So even if Jest did not come with create-react-app I would still leverage jest on greenfield applications (I do have loads of experience with TAP based runners like ava and tape, as well as mocha). Coverage on this app did hit 100% for the library code I would extract to an npm package. I did not test `App.js` or `api.js` as they are just used to demo the Composer and ArrowKeyHandler components.

### Coverage
```
---------------------|----------|----------|----------|----------|----------------|
File                 |  % Stmts | % Branch |  % Funcs |  % Lines |Uncovered Lines |
---------------------|----------|----------|----------|----------|----------------|
 src/ArrowKeyHandler |      100 |      100 |      100 |      100 |                |
  index.js           |      100 |      100 |      100 |      100 |                |
 src/composer        |      100 |    87.23 |      100 |      100 |                |
  List.js            |      100 |       75 |      100 |      100 |                |
  ListItem.js        |      100 |    85.71 |      100 |      100 |                |
  index.js           |      100 |    88.89 |      100 |      100 |                |
 src/utils           |      100 |    92.59 |      100 |      100 |                |
  index.js           |      100 |    92.59 |      100 |      100 |                |
---------------------|----------|----------|----------|----------|----------------|
```

## File Structure
```
├── public
│   ├── avatar.jpg
│   ├── camera-icon.png
│   ├── favicon.ico
│   └── index.html
├── src
│   ├── ArrowKeyHandler
│   │   ├── index.js
│   │   └── index.test.js
│   ├── composer
│   │   ├── __snapshots__
│   │   │   ├── List.test.js.snap
│   │   │   └── ListItem.test.js.snap
│   │   ├── List.js
│   │   ├── List.test.js
│   │   ├── ListItem.js
│   │   ├── ListItem.test.js
│   │   ├── index.js
│   │   └── index.test.js
│   ├── utils
│   │   ├── index.js
│   │   └── index.test.js
│   ├── App.css
│   ├── App.js
│   ├── App.test.js
│   ├── api.js
│   └── index.js
├── README.md
├── package.json
├── test.js
└── yarn.lock
```

## Possible Performance Improvements

1. Currently Composer takes a prop to specify how many suggestions to make based on your mention text (defaulted to 2). If that default number were a very large number say like 1 million we would want to leverage virtual scrolling to load all of those without choking the browser. With virtual scrolling we only load the components in the view port and eagerly fetch a few of the upcoming ones. Normally when I need virtual scrolling I go to [react-virtualized](https://bvaughn.github.io/react-virtualized/) as it is incredibly flexible, well tested, and actively maintained.

2. Another improvement would be leveraging service workers for caching of API requests for offline usage. That is something that is preferred to be handled by the consumer of this utility if possible and in which case the user can set `cache={false}` as a prop to composer.

3. Using observables might also be something to consider. It's certainly something interesting to try either way. I would imagine it would be mapping the eventStream$ to get the event.keyCode, performing a scan to create a finite state machine to check if entering a mention, and followed by emitting the new state.  It does however get tricky in implementation details I'm sure.


