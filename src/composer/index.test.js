import React from 'react';
import { mount, shallow } from 'enzyme';
import Composer, { MODES } from './';

jest.useFakeTimers();

const fakeSuggestions = [
  { id: 1, name: 'test', username: 'test', avatar: 'test' },
  { id: 2, name: 'test2', username: 'test2', avatar: 'test2' },
];

const fetchMock = jest
  .fn()
  .mockReturnValue({ then: cb => cb(fakeSuggestions) });

const itemRenderer = ({ id, onSelect, username }) => {
  return (
    <div className="listItem" onClick={() => onSelect(username)} key={id}>
      {username}
    </div>
  );
};

const renderComposer = props => (
  <Composer suggestionRenderer={itemRenderer} {...props} />
);

describe('Composer', () => {
  it('should render', () => {
    const wrapper = shallow(renderComposer());
    expect(wrapper.find('.mention-composer-container').length).toEqual(1);
  });

  describe('composing messages', () => {
    it('it should allow typing in the textarea without mentions', () => {
      const wrapper = shallow(renderComposer());
      wrapper
        .find('textarea')
        .simulate('change', { target: { value: 'this is a test' } });

      expect(wrapper.state().message).toEqual('this is a test');
      expect(wrapper.state().suggestions).toEqual([]);
      expect(wrapper.state().mode).toEqual(MODES.INPUT);
    });

    it('it should enter mention mode when a mention is typed', () => {
      const wrapper = shallow(
        renderComposer({ debounceTime: 0, fetch: fetchMock }),
      );
      wrapper.find('textarea').simulate('change', {
        target: { value: 'this is a @test', selectionStart: 15 },
      });

      expect(wrapper.state().message).toEqual('this is a @test');
      expect(wrapper.state().mode).toEqual(MODES.MENTION);
    });

    it('should leave mention mode after mention', () => {
      const wrapper = shallow(
        renderComposer({ debounceTime: 0, fetch: fetchMock }),
      );
      wrapper.find('textarea').simulate('change', {
        target: { value: 'this is a @test and', selectionStart: 17 },
      });

      expect(wrapper.state().message).toEqual('this is a @test and');
      expect(wrapper.state().mode).toEqual(MODES.INPUT);
    });
  });

  describe('dom events', () => {
    it('should exit mention mode on ESCAPE', () => {
      const wrapper = shallow(
        renderComposer({ debounceTime: 0, fetch: fetchMock }),
      );
      wrapper.setState({ message: 'this is a @test', mode: MODES.MENTION });
      wrapper
        .find('textarea')
        .simulate('keyDown', { key: 'Escape', preventDefault: jest.fn() });
      expect(wrapper.state().message).toEqual('this is a @test');
      expect(wrapper.state().mode).toEqual(MODES.INPUT);
    });

    it('should call props.resetFocus on mouseOver', () => {
      const resetFocusMock = jest.fn();
      const wrapper = shallow(
        renderComposer({ resetFocus: resetFocusMock, fetch: fetchMock }),
      );
      wrapper.find('.suggestions-container').simulate('mouseover');
      expect(resetFocusMock.mock.calls.length).toEqual(1);
    });
  });

  describe('cache', () => {
    it('should cache fetch requets by default', () => {
      const fetchMock = jest
        .fn()
        .mockReturnValue({ then: cb => cb(['fakeData']) });
      const wrapper = shallow(
        renderComposer({ debounceTime: 0, fetch: fetchMock }),
      );

      wrapper.instance().fetch('test');
      wrapper.instance().fetch('test');
      wrapper.instance().fetch('test2');

      expect(fetchMock.mock.calls.length).toEqual(2);
    });

    it('should not cache fetch requets if prop is false', () => {
      const fetchMock = jest
        .fn()
        .mockReturnValue({ then: cb => cb(['fakeData']) });
      const wrapper = shallow(
        renderComposer({ cache: false, debounceTime: 0, fetch: fetchMock }),
      );
      wrapper.instance().fetch('test');
      wrapper.instance().fetch('test');
      wrapper.instance().fetch('test2');

      expect(fetchMock.mock.calls.length).toEqual(3);
    });
  });

  describe('suggestions', () => {
    it('should render fetched suggestions and click on one', () => {
      const wrapper = mount(
        renderComposer({ debounceTime: 0, fetch: fetchMock }),
      );

      wrapper.find('textarea').simulate('change', {
        target: { value: 'this is a @test and', selectionStart: 14 },
      });

      const listItems = wrapper.find('.listItem');

      expect(wrapper.instance().state.suggestions).toEqual(fakeSuggestions);
      expect(listItems.length).toEqual(2);
      listItems.last().simulate('click');
      expect(wrapper.instance().state.message).toEqual('this is  @test2  and');
    });

    it('select focused index on key "Enter"', () => {
      const wrapper = mount(
        renderComposer({ debounceTime: 0, fetch: fetchMock, focusIndex: 1 }),
      );

      wrapper.instance().setState({
        mode: MODES.MENTION,
        message: '',
        suggestions: fakeSuggestions,
      });

      wrapper
        .find('textarea')
        .simulate('keyDown', { key: 'Enter', preventDefault: jest.fn() });

      expect(wrapper.instance().state.message).toEqual('@test2');
    });

    it('not do anything on unhandled keyDown', () => {
      const wrapper = mount(
        renderComposer({ debounceTime: 0, fetch: fetchMock, focusIndex: 1 }),
      );

      wrapper
        .find('textarea')
        .simulate('keyDown', { key: 'a', preventDefault: jest.fn() });

      expect(wrapper.instance().state.message).toEqual('');
    });
  });
});
