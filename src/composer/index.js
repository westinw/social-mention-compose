import React, { Component, PropTypes } from 'react';
import invariant from 'invariant';
import debounce from 'lodash.debounce';
import List from './List';
import {
  findNextSpace,
  isInMention,
  lastMentionWord,
  replaceCurrentWord,
  identity,
  resolveImmediate,
} from '../utils';

/**
 * Is the app in text edit or mention suggesting mode.
 * Using symbols to prevent magic numbers floating around
 */
export const MODES = {
  INPUT: Symbol(),
  MENTION: Symbol(),
};

const initialMentionState = {
  suggestions: [],
  mode: MODES.INPUT,
};

export default class TweetComposer extends Component {
  static propTypes = {
    /**
     * Whether or not to cache fetch calls in a Map
     */
    cache: PropTypes.bool,
    /**
     * The fetch prop is debounced so supply the time to debounce
     */
    debounceTime: PropTypes.number,
    /**
     * The method to call to get usernames. Must return a promise
     */
    fetch: PropTypes.func,
    /**
     * Which suggestion to give a focused class + click handler. Used with ArrowKeyHandler currently.
     */
    focusIndex: PropTypes.number,
    /**
     * What character to use to start a mention. Defaults to '@'
     */
    mentionChar: PropTypes.string,
    /**
     * Supply a custom keyHandler to be called in addition with the default one.
     */
    onKeyDown: PropTypes.func,
    /**
     * Provide a hook for when focus on suggestions is changed
     */
    resetFocus: PropTypes.func,
    /**
     * How many suggestions to include in the autocomplete drop down.
     */
    suggestionCount: PropTypes.number,
    /**
     * Function to render the component for each suggestion
     */
    suggestionRenderer: PropTypes.func,
  };

  static defaultProps = {
    cache: true,
    debounceTime: 300,
    fetch: resolveImmediate,
    focusIndex: -1,
    mentionChar: '@',
    onKeyDown: identity,
    resetFocus: identity,
    suggestionCount: 2,
    suggestionRenderer: identity,
  };

  state = {
    ...initialMentionState,
    /**
     * User typed message
     */
    message: '',
    /**
     * Current position of cursor in the textarea
     */
    cursorPosition: 0,
  };

  constructor(props) {
    super(props);
    this.cache = new Map(); // use Map data structure for performance

    // Prop Checks
    invariant(
      this.props.mentionChar.length === 1,
      'Mention char must only be a single character!',
    );
    invariant(
      this.props.suggestionRenderer,
      'You must supply a suggestionRenderer function!',
    );

    // only debounce if needed
    if (this.props.debounceTime > 0) {
      this.fetch = debounce(this.fetch, this.props.debounceTime).bind(this);
    } else {
      this.fetch = this.fetch.bind(this);
    }

    this.handleChange = this.handleChange.bind(this);
    this.handleKeyDown = this.handleKeyDown.bind(this);
    this.handleMouseOver = this.handleMouseOver.bind(this);
    this.handleUserSelect = this.handleUserSelect.bind(this);
    this.resetFocus = this.resetFocus.bind(this);
  }

  fetch(username) {
    const limit = this.props.suggestionCount;
    const cache = this.props.cache ? this.cache.get(username) : false;

    if (cache) {
      this.setState({ suggestions: cache });
    } else {
      this.props.fetch(username).then(users => {
        const usersSlice = users.slice(0, limit);
        if (this.props.cache) {
          this.cache.set(username, usersSlice);
        }
        this.setState({
          suggestions: usersSlice,
        });
      });
    }
  }

  handleChange(e) {
    const { value, selectionStart } = e.target;
    const inMention = isInMention(
      value,
      selectionStart - 1,
      this.props.mentionChar,
    );
    const currentWord = lastMentionWord(
      value.slice(0, selectionStart),
      this.props.mentionChar,
    );
    const newMode = inMention ? MODES.MENTION : MODES.INPUT;

    this.setState(
      {
        message: value,
        cursorPosition: selectionStart,
        mode: newMode,
        suggestions: value ? this.state.suggestions : [],
      },
      () => {
        // run after the change to state for new text so as not
        // to delay users typing at all if something fishy is
        // happening in user supplied fetch
        if (newMode === MODES.MENTION && currentWord.length > 1) {
          this.fetch(currentWord);
        }
      },
    );
  }

  handleKeyDown(e) {
    /**
     * Do not allow carriage returns for now
     */
    if (['Enter', 'Escape'].includes(e.key)) {
      e.preventDefault();
    }

    if (this.props.onKeyDown) {
      this.props.onKeyDown(e);
    }
    switch (e.key) {
      /**
       * Escape and space both clear the suggestions dropDown from state
       */
      case 'Escape':
      case ' ': {
        this.setState(initialMentionState);
        break;
      }
      case 'Enter': {
        /**
         * focusIndex is set to a value (not -1) and we are in mention mode
         * allow the user to select the suggestion by pressing enter.
         */
        if (~this.props.focusIndex && this.state.mode === MODES.MENTION) {
          const focusIndex = this.props.focusIndex;
          const username = this.state.suggestions[focusIndex].username;
          this.handleUserSelect(username);
        }
        break;
      }
      default:
        break;
    }
  }

  handleUserSelect(username) {
    this.setState(
      {
        message: replaceCurrentWord(
          this.state.message,
          `${this.props.mentionChar}${username}`,
          this.state.cursorPosition,
        ),
        mode: MODES.INPUT,
        suggestions: [],
      },
      () => {
        /**
         * Update cursor position after state.message has been replaced
         */
        const newPosition = findNextSpace(
          this.state.message,
          this.state.cursorPosition,
        );
        this.resetFocus(); // call resetFocus prop if supplied
        this.textarea.focus(); // refocus textarea to continue typing
        this.textarea.setSelectionRange(newPosition, newPosition); // update cursor pos
      },
    );
  }

  handleMouseOver() {
    this.resetFocus();
  }

  /* if resetFocus prop was passed in call it */
  resetFocus() {
    if (this.props.resetFocus) {
      this.props.resetFocus();
    }
  }

  /* helper to determine whether to show suggestions drop down */
  get shouldShowSuggestions() {
    return this.state.mode === MODES.MENTION && this.state.suggestions.length;
  }

  /* helper to render suggestions list */
  get suggestions() {
    return (
      <List
        data={this.state.suggestions}
        focusIndex={this.props.focusIndex}
        onSelect={this.handleUserSelect}
        itemRenderer={this.props.suggestionRenderer}
      />
    );
  }

  render() {
    const suggestionsClass = this.shouldShowSuggestions ? '' : 'hidden';
    return (
      <div className="mention-composer-container">
        <div className="mention-textarea-container">
          <textarea
            onKeyDown={this.handleKeyDown}
            onChange={this.handleChange}
            placeholder="What's happening?"
            value={this.state.message}
            ref={textarea => this.textarea = textarea}
          />
        </div>
        <div
          className={`suggestions-container ${suggestionsClass}`}
          onMouseOver={this.handleMouseOver}
        >
          {this.shouldShowSuggestions ? this.suggestions : null}
        </div>
      </div>
    );
  }
}
