import React from 'react';
import { shallow } from 'enzyme';
import renderer from 'react-test-renderer';
import ListItem from './ListItem';

describe('components', () => {
  describe('ListItem', () => {
    it('renders correctly', () => {
      const tree = renderer
        .create(<ListItem username="test" name="Westin" focus />)
        .toJSON();
      expect(tree).toMatchSnapshot();
    });

    it('renders with 3 info classes even with no props', () => {
      const wrapper = shallow(<ListItem />);
      expect(wrapper.find('.info').length).toEqual(3);
    });

    it('not fail with no click handler', () => {
      const wrapper = shallow(<ListItem />);
      wrapper.find('.listItem').simulate('click');
      expect(wrapper.find('.listItem').length).toEqual(1);
    });

    it('fires onSelect correctly', () => {
      const mockClickHandler = jest.fn();
      const wrapper = shallow(
        <ListItem onSelect={mockClickHandler} username="test" />,
      );
      wrapper.find('.listItem').simulate('click');
      expect(mockClickHandler.mock.calls).toEqual([['test']]);
    });
  });
});
