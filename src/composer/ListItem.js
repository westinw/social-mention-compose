import React, { PropTypes } from 'react';
import { identity } from '../utils';

/**
 * Sample List item to use as suggestionRenderer
 */
const ListItem = (
  {
    avatar = '',
    focused = false,
    name = '',
    username = '',
    onSelect = identity,
  },
) => (
  <li
    className={`listItem ${focused ? 'focus' : ''}`}
    onClick={() => onSelect(username)}
  >
    <div className="info">
      <img className="avatar" role="presentation" src={avatar} />
    </div>
    <div className="info"><span>{name}</span></div>
    <div className="info"><span className="username">@{username}</span></div>
  </li>
);

ListItem.propTypes = {
  avatar: PropTypes.string,
  focused: PropTypes.bool,
  name: PropTypes.string,
  username: PropTypes.string,
  onSelect: PropTypes.func,
};

export default ListItem;
