import React, { PropTypes } from 'react';
import { identity } from '../utils';

/**
 * Simple List component to render the suggestions.
 * passes the spread of datum down as users can supply their
 * own itemRenderer and we don't want to restrict the props passed
 */
const List = (
  {
    data = [],
    focusIndex = 0,
    onSelect = identity,
    itemRenderer = identity, // required
  },
) => (
  <ul>
    {data.map((datum, index) => itemRenderer({
      ...datum,
      focused: focusIndex === index,
      onSelect: onSelect,
      key: datum.id,
    }))}
  </ul>
);

List.propTypes = {
  data: PropTypes.array,
  focusIndex: PropTypes.number,
  onSelect: PropTypes.func,
  itemRenderer: PropTypes.func,
};

export default List;
