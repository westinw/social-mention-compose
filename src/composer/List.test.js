import React from 'react';
import { shallow } from 'enzyme';
import renderer from 'react-test-renderer';
import List from './List';

const data = [
  { username: 'username1', name: 'user1', avatar: 'avatar1', id: 1 },
  { username: 'username2', name: 'user2', avatar: 'avatar2', id: 2 },
  { username: 'username3', name: 'user3', avatar: 'avatar3', id: 3 },
];

const itemRenderer = ({ id }) => <div key={id} />;

describe('components', () => {
  describe('List', () => {
    it('renders correctly', () => {
      const tree = renderer
        .create(<List data={data} focusIndex={1} itemRenderer={itemRenderer} />)
        .toJSON();
      expect(tree).toMatchSnapshot();
    });

    it('renders correctly with no props', () => {
      const wrapper = shallow(<List itemRenderer={itemRenderer} />);
      expect(wrapper.children().length).toEqual(0);
    });

    it('renders allow click with click handler props', () => {
      const wrapper = shallow(<List data={data} itemRenderer={itemRenderer} />);
      expect(wrapper.children().length).toEqual(3);
    });
  });
});
