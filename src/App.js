import React, { Component } from 'react';
import Composer from './composer';
import ArrowKeyHandler from './ArrowKeyHandler';
import { fetchUsers } from './api';
import ListItem from './composer/ListItem';

import './App.css';

class App extends Component {
  fetchUsers(username) {
    return fetchUsers(username);
  }

  itemRenderer({ avatar, focused, name, onSelect, username }) {
    // using the sample renderer that comes with the composer module
    return (
      <ListItem
        key={username}
        avatar={avatar}
        focused={focused}
        name={name}
        onSelect={onSelect}
        username={username}
      />
    );
  }

  render() {
    return (
      <div className="App" style={{ marginTop: '50px' }}>
        <div id="base">
          <div className="tweet-composer">
            <div className="edit">
              <div>
                <img className="avatar" role="presentation" src="/avatar.jpg" />
              </div>
              <div className="text-area-container">
                <ArrowKeyHandler>
                  {({ focusIndex, onKeyDown, resetFocus }) => (
                    <Composer
                      focusIndex={focusIndex}
                      onKeyDown={onKeyDown}
                      resetFocus={resetFocus}
                      fetch={this.fetchUsers}
                      suggestionRenderer={this.itemRenderer}
                    />
                  )}
                </ArrowKeyHandler>
              </div>
            </div>
            <div className="toolbar">
              <div className="toolbar-left">
                <img className="icon" alt="upload" src="/camera-icon.png" />
              </div>
              <div className="toolbar-right">
                <button>Tweet</button>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default App;
