import * as utils from './';

describe('utils', () => {
  describe('identity', () => {
    it('should be an identity function...', () => {
      expect(utils.identity('test')).toEqual('test');
    });
  });

  describe('resolveImmediate', () => {
    it('should immediately resolve default value', () => {
      return utils.resolveImmediate().then(i => expect(i).toEqual([]));
    });

    it('should immediately resolve any identity', () => {
      return utils
        .resolveImmediate('test')
        .then(i => expect(i).toEqual('test'));
    });
  });

  describe('lastMentionWord', () => {
    it('should return the mention with a happy string', () => {
      expect(utils.lastMentionWord('this is a @test')).toEqual('test');
    });

    it('should return the mention while it is sandwhiched', () => {
      expect(utils.lastMentionWord('this is a @test and such')).toEqual('test');
    });

    it('should return empty string with no mention', () => {
      expect(utils.lastMentionWord('this is a test')).toEqual('');
    });

    it('should return empty string with am empty string', () => {
      expect(utils.lastMentionWord('')).toEqual('');
    });
  });

  describe('getWordAtCursor', () => {
    it('should return the word with a happy string', () => {
      expect(utils.getWordAtCursor('this is a test.', 12)).toEqual('test');
    });

    it('should return the input string with a 0 space string', () => {
      expect(utils.getWordAtCursor('thisisatest.', 12)).toEqual('thisisatest');
    });

    it('should return an empty string with cursor at a space', () => {
      expect(utils.getWordAtCursor('this is a test ', 7)).toEqual('');
    });

    it('should FAIL if word is empty', () => {
      expect(utils.getWordAtCursor('', 0)).toEqual('');
    });

    it('should FAIL if position is bad', () => {
      expect(utils.getWordAtCursor('thisisatest.', 100)).toEqual('');
      expect(utils.getWordAtCursor('thisisatest.', -100)).toEqual('');
    });
  });

  describe('replaceCurrentWord', () => {
    it('should replace the word while in the middle of a string', () => {
      expect(
        utils.replaceCurrentWord('this is a test and stuff', 'replacement', 14),
      ).toEqual('this is a replacement and stuff');
    });

    it('should replace the word at the end of a string', () => {
      expect(
        utils.replaceCurrentWord('this is a test', 'replacement', 14),
      ).toEqual('this is a replacement');
    });
  });

  describe('isMention', () => {
    it('should return with a happy string', () => {
      expect(utils.isMention('@test')).toEqual(true);
    });

    it('should return false without a mention', () => {
      expect(utils.isMention('test')).toEqual(false);
    });

    it('should return with a custom mention char', () => {
      expect(utils.isMention('#test', '#')).toEqual(true);
    });

    it('should return false with an empty string', () => {
      expect(utils.isMention('')).toEqual(false);
    });

    it('should return false with space around', () => {
      expect(utils.isMention(' @test ')).toEqual(false);
    });
  });

  describe('findNextSpace', () => {
    it('should return with a happy string', () => {
      expect(utils.findNextSpace('this is a test', 2)).toEqual(4);
    });

    it('should return if at end of string', () => {
      expect(utils.findNextSpace('this', 4)).toEqual(4);
    });

    it('should return 0 with an empty string', () => {
      expect(utils.findNextSpace('', 0)).toEqual(0);
    });

    it('should return 0 with out of bounds index', () => {
      expect(utils.findNextSpace('a', 4)).toEqual(0);
    });
    describe('isInMention', () => {
      it('should return true if in mention', () => {
        expect(utils.isInMention('@test', 2)).toEqual(true);
      });
      it('should return false if not in mention', () => {
        expect(utils.isInMention('test', 2)).toEqual(false);
      });
    });
  });
});
