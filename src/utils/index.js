/**
 * If these utils were used in an app and not bundled with the composer
 * in a private npm package, I would consider splitting them out to
 * each utility function having its own file and using a default export.
 * By splitting them out like that it helps folks to find them and not either
 * recreate them or grab one online as they can see them in their file tree.
 *
 * For this demo, I think it is fine to keep as one file as it's trivial to split.
 */

/**
 * Identity function. Doesn't do anything
 */
export const identity = i => i;

/**
 * Default methods for defaultProps
 */
export const resolveImmediate = (i = []) => Promise.resolve(i);

/**
 * Find the next space from current cursor position
 * @param {string} str string to search
 * @param {number} pos the position to start searching
 */
export const findNextSpace = (str, pos) => {
  if (pos > str.length) {
    return 0;
  }

  let nextPosition = pos;
  while (nextPosition !== str.length && str[nextPosition] !== ' ') {
    ++nextPosition;
  }

  return nextPosition;
};

/**
 * Get the last mention in a string
 * @param {string} str any string
 */
export const lastMentionWord = (str, mentionChar = '@') => {
  if (!str.includes(mentionChar)) {
    return '';
  }
  const substring = str.slice(str.lastIndexOf(mentionChar) + 1);
  const firstSpace = substring.indexOf(' ');
  const endOfMention = firstSpace >= 0 ? firstSpace : str.length;
  return substring.slice(0, endOfMention);
};

/**
 * Get the word that the supplied index is within.
 * if the index is out of bounds or a space return an empty string
 * @param {string} str the string to search
 * @param {number} pos the position in the string to find the word
 */
export const getWordAtCursor = (str = '', pos = 0) => {
  if (pos < 0 || pos > str.length) {
    return '';
  }

  if (str[pos] === ' ') {
    return '';
  }

  const matches = /\S+$/.exec(str.slice(0, str.indexOf(' ', pos)));
  return matches && matches.length ? matches[0] : '';
};

// Twitter.com doesn't auto complete inside a mention if you type in the middle
// thus for time constraints that can be a future enhancement.
// Twitter.com however does continue to provide autocomplete if you resume
// typing at the end of the tweet
export const replaceCurrentWord = (str, replacement, pos) => {
  const word = getWordAtCursor(str, pos - 1);
  const newWord = pos === str.length ? replacement : ` ${replacement} `;

  return str.substring(0, pos - word.length - 1) +
    newWord +
    str.substring(pos + 1);
};

/**
 * Test if the current string is a mention by checking
 * that it starts with a mention char and has no spaces.
 * @param {string} str string
 * @param {char} mentionChar start character of a mention. defaults to '@'
 */
export const isMention = (str, mentionChar = '@') =>
  str.startsWith(mentionChar) && !str.includes(' ');

/**
 * Compose getWordAtCursor and isMention as they will be
 * used together often
 */
export const isInMention = (str, pos, mentionChar) =>
  isMention(getWordAtCursor(str, pos), mentionChar);
