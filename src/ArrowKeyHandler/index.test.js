import React from "react";
import { mount } from "enzyme";
import ArrowKeyHandler from "./";

const Child = ({ focus, onKeyDown, resetFocus }) => (
  <div className="test">
    <input onKeyDown={onKeyDown} />
    <button onClick={resetFocus}>reset</button>
  </div>
);

const mountComponent = () => {
  return mount(
    <ArrowKeyHandler>
      {({ onKeyDown, resetFocus }) => (
        <Child onKeyDown={onKeyDown} resetFocus={resetFocus} />
      )}
    </ArrowKeyHandler>
  );
};

describe("ArrowKeyHandler", () => {
  it("should render it's child", () => {
    const wrapper = mountComponent();
    expect(wrapper.find(".test").length).toEqual(1);
  });

  it("simulates arrow down", () => {
    const wrapper = mountComponent();
    const textArea = wrapper.find("input");
    expect(textArea.length).toEqual(1);
    wrapper.setState({ focusIndex: 3 });
    textArea.simulate("keyDown", { key: "ArrowDown", keyCode: 38 });
    expect(wrapper.state().focusIndex).toEqual(4);
    // do not let it go above max
    textArea.simulate("keyDown", { key: "ArrowDown", keyCode: 38 });
    expect(wrapper.state().focusIndex).toEqual(4);
  });

  it("simulates arrow up", () => {
    const wrapper = mountComponent();
    const textArea = wrapper.find("input");
    expect(textArea.length).toEqual(1);
    wrapper.setState({ focusIndex: 0 });
    textArea.simulate("keyDown", { key: "ArrowUp", keyCode: 40 });
    expect(wrapper.state().focusIndex).toEqual(-1);
    // do not let it go below min
    textArea.simulate("keyDown", { key: "ArrowUp", keyCode: 40 });
    expect(wrapper.state().focusIndex).toEqual(-1);
  });

  it("ignores other events", () => {
    const wrapper = mountComponent();
    const textArea = wrapper.find("input");
    expect(textArea.length).toEqual(1);
    wrapper.setState({ focusIndex: 0 });
    textArea.simulate("keyDown", { key: "Backspace" });
    expect(wrapper.state().focusIndex).toEqual(0);
  });

  it("resets state", () => {
    const wrapper = mountComponent();
    const button = wrapper.find("button");
    wrapper.setState({ focusIndex: 2 });
    button.simulate("click");
    expect(wrapper.state().focusIndex).toEqual(-1);
  });
});
