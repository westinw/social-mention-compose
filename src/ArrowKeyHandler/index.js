import { Component, PropTypes } from "react";

const initialState = {
  focusIndex: -1
};

/**
 * ArrowKeyHandler uses Children as Function pattern
 * to allow maximum reuse.
 */
export default class ArrowKeyHandler extends Component {
  static defaultProps = { min: -1, max: 4 };
  state = {
    ...initialState
  };

  static propTypes = {
    /**
     * The minimum focus index to allow.
     */
    min: PropTypes.number,
    /**
     * The maximum focus index to allow.
     */
    max: PropTypes.number
  };

  constructor(props) {
    super(props);
    this.handleKeyDown = this.handleKeyDown.bind(this);
    this.handleReset = this.handleReset.bind(this);
  }

  updateFocus(newValue) {
    const min = this.props.min;
    const max = this.props.max;

    // don't allow the user to leave the range allowed
    let focusIndex = newValue;
    if (newValue > max) {
      focusIndex = max;
    } else if (newValue < min) {
      focusIndex = min;
    }

    this.setState({ focusIndex });
  }

  handleKeyDown(e) {
    switch (e.key) {
      case "ArrowDown": {
        e.preventDefault();
        this.updateFocus(this.state.focusIndex + 1);
        return;
      }
      case "ArrowUp": {
        e.preventDefault();
        this.updateFocus(this.state.focusIndex - 1);
        return;
      }
      default:
        return;
    }
  }

  handleReset() {
    this.setState(initialState);
  }

  render() {
    return this.props.children({
      focusIndex: this.state.focusIndex,
      onKeyDown: this.handleKeyDown,
      resetFocus: this.handleReset
    });
  }
}
