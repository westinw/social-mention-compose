const API_BASE = process.env.REACT_APP_API_BASE || 'http://localhost:3001';

export const fetchUsers = username => {
  if (!username) {
    return Promise.resolve([]);
  }

  return fetch(`${API_BASE}/twitter/user/search?username=${username}`)
    .then(d => d.json())
    .then(d => d.users.map(u => ({
      id: u.id,
      name: u.name,
      username: u.screen_name,
      avatar: u.profile_image_url,
    })));
};
