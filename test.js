var ta_input = document.getElementById('ta_input');
var ta_state = document.getElementById('ta_state');
var ta_result = document.getElementById('ta_result');

function emits(who, who_) {
  return function(x) {
    who.innerHTML = [who.innerHTML, who_ + ' emits ' + JSON.stringify(x)].join(
      '\n',
    );
  };
}

function noop() {}

const MODE_AUTO = 1, MODE_NORMAL = 0;

var keyUp$ = Rx.Observable.fromEvent(ta_input, 'keyup').map(function(ev) {
  return ev.keyCode;
});

var stateMachine$ = keyUp$
  .scan(
    function(state, keyCode) {
      console.log(keyCode);

      if (keyCode === 50) {
        state.mode = MODE_AUTO;
        state.prefix = '';
        return state;
      }

      if (keyCode == 32) {
        state.mode = MODE_NORMAL;
      }

      if (state.mode == MODE_AUTO) {
        console.log('in auto mode');
        state.prefix += String.fromCharCode(keyCode);
      }

      console.log('prefix ', state.prefix);

      return state;
    },
    { mode: MODE_NORMAL, prefix: '' },
  )
  .do(emits(ta_state, 'state'))
  .filter(function(state) {
    return state.mode;
  })
  .do(emits(ta_result, 'found'));

stateMachine$.subscribe(noop);
